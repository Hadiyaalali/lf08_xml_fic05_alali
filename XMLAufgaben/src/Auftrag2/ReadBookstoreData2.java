package Auftrag2;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */



public class ReadBookstoreData2 {

	public static void main(String[] args) {

DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {

			  DocumentBuilder builder = factory.newDocumentBuilder();
	    	  Document doc = builder.parse("buchhandlung2.xml");
	    	  Node titel = doc.getElementsByTagName("titel").item(0);
	    	 
	    	  Element titelElem = (Element) titel;
	          System.out.println(titelElem.getNodeName() + ": " + titelElem.getTextContent());
	          Node autor = doc.getElementsByTagName("autor").item(0);
	          Element autorElem = (Element) autor;
	          System.out.println(autorElem.getNodeName() + ": " + autorElem.getTextContent());
	          
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

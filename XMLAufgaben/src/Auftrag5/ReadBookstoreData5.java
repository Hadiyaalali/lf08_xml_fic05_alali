package Auftrag5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */

public class ReadBookstoreData5 {

	public static void main(String[] args) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung4_5.xml");
			NodeList buch = doc.getElementsByTagName("buch");

			for (int i = 0; i < buch.getLength(); i++) {

				Node knoten = buch.item(i);
				Element buchElem = (Element) knoten;
			
				Node titel = buchElem.getElementsByTagName("titel").item(0);
				Element titelElem = (Element) titel;
//		        System.out.println(titelElem.getNodeName() + ": " + titelElem.getTextContent());
				NodeList autor = buchElem.getElementsByTagName("autor");
				
				if(titelElem.getTextContent().equals("XQuery Kick Start")) {
					  System.out.println("Buchtitel: " + titelElem.getTextContent());
					  System.out.println("Autoren: ");
					for(int x = 0; x<autor.getLength(); x++) {
			    		  
			    		  Node node = autor.item(x);
			    		  Element autorElem = (Element) node;
			    		  
			    		  System.out.println("\t" + (x+1) + ". " + autorElem.getNodeName() + ": " + autorElem.getTextContent());
			    	  }
					  
				}
			
			
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			 DocumentBuilder builder = factory.newDocumentBuilder();
	    	  Document doc = builder.parse("buchhandlung3.xml");
	    	  Node titel = doc.getElementsByTagName("titel").item(0);
	    	
	    	  Element titelElem = (Element) titel;
	          System.out.println(titelElem.getNodeName() + ": " + titelElem.getTextContent());
	          
	          Node vorname = doc.getElementsByTagName("vorname").item(0);
	          Element vornameElem = (Element) vorname;
	          System.out.println(vornameElem.getNodeName() + ": " + vornameElem.getTextContent());
	          
	          Node nachname = doc.getElementsByTagName("nachname").item(0);
	          Element nachnameElem = (Element) nachname;
	          System.out.println(nachnameElem.getNodeName() + ": " + nachnameElem.getTextContent());
	          
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}

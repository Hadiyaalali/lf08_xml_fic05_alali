package Auftrag4;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData4 {

	public static void main(String[] args) {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		 try { DocumentBuilder builder = factory.newDocumentBuilder();
    	  Document doc = builder.parse("buchhandlung4_5.xml");
    	  NodeList titel = doc.getElementsByTagName("titel");
    	 
    	  for(int i = 0; i<titel.getLength(); i++) {
    		  
    		  Node knoten = titel.item(i);
    		  Element titelElem = (Element) knoten;
    		  
    		  System.out.println((i+1) + ". " + titelElem.getNodeName() + ": " + titelElem.getTextContent());
    	  }
			

		 }
		 catch(IOException e) {
			 e.printStackTrace();
			 
		 } catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}

}

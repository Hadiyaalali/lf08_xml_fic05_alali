package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class ReadBookstoreData1 {

  public static void main(String[] args) {

  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    try {
    	  DocumentBuilder builder = factory.newDocumentBuilder();
    	  Document doc = builder.parse("buchhandlung1.xml");
    	  Node titel = doc.getElementsByTagName("titel").item(0);
    	 
    	  Element titelElem = (Element) titel;
          System.out.println(titelElem.getNodeName() + ": " + titelElem.getTextContent());
    	  
    	
    } catch (Exception e) {
      e.printStackTrace();
    } 

  }

}
